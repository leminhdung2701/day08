<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="css.css" type="text/css">

<body>

    <?php
        session_start();
        $date =  date("d/m/Y", strtotime($_SESSION["date"]));
        $img = $_SESSION["imageInput"];
    ?>

    <div class="login">
        <form method="POST" action="">
            <div class="row">
                <div class="column_label div_label_confirm">Họ và tên
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<div class="text">' . $_SESSION["name"] . ' </div> '
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Giới tính
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<div class="text">' . $_SESSION["gender"] . ' </div> '
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Phân khoa
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <div class="column_input">
                        <?php
                            echo '<div class="text">' . $_SESSION["khoa"] . ' </div> '
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Ngày sinh
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<div class="text">' . $date . ' </div> '
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Địa chỉ</div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<div class="text">' . $_SESSION["address"] . ' </div> '
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label_confirm">Hình ảnh</div>
                <div class="column"> </div>
                <div class="column_input">
                    <?php
                        echo '<img class="text" src="'.$img.'" width="50%">'
                    ?>
                </div>
            </div>

            <div class="">
                <input class="center div_button" type="submit" value="Xác nhận" name="submit">
            </div>

        </form>

    </div>
</body>
